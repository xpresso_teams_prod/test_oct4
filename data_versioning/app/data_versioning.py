import os
import sys
import types
import numpy
import keras
import pickle
import pandas
import marshal
import seaborn
import autofeat
import warnings
import matplotlib
import tensorflow
import matplotlib.pyplot
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from xpresso.ai.core.data.exploration import Explorer
from autofeat import FeatureSelector, AutoFeatRegressor
from xpresso.ai.core.data.automl import StructuredDataset
from xpresso.ai.core.data.visualization import Visualization
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = data_versioning
## $xpr_param_component_type = pipeline_job
df_cleaned.to_csv("cleaned_data.csv", encoding='utf-8', index=False)
data_config2 = {"type": "FS",
               "data_source": "Local",
               "options": {"sep": " ", "header": None},
               "path": "/home/jovyan/predictive_maintenance/notebooks/cleaned_data.csv"}

# #push the dataset
repo_manager.push_dataset(repo_name="demo_predictive_maintenance", branch_name="turbofan_data",  
                          dataset=my_dataset, description="cleaned data", type="data")
